package techunited.jy.star;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class gameOver extends AppCompatActivity {

    TextView time_text, level_text,score_text,serverdata;
    int sec, min, milsec;
    long time_past;
    RelativeLayout bg_layout;
    LinearLayout setting_page;
    ImageView background;
    boolean rotating=false;

    LayoutInflater mInflater;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        mInflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/mechanik_regular.ttf");
        time_text = (TextView) findViewById(R.id.time);
        level_text = (TextView) findViewById(R.id.level);
        score_text = (TextView)findViewById(R.id.score);

        bg_layout=(RelativeLayout)findViewById(R.id.bg_layout);
        background=(ImageView)findViewById(R.id.background);

        time_text.setTypeface(font);
        level_text.setTypeface(font);
        score_text.setTypeface(font);
        //serverdata= (TextView)findViewById(R.id.serverdata);


        Intent intent = getIntent();

        pref = getSharedPreferences("pref",MODE_PRIVATE);
        editor = pref.edit();

        level_text.setText(""+intent.getIntExtra("level", 0));
        time_past=intent.getLongExtra("time_past",0);


        if(intent.getBooleanExtra("new_h_score",false)){
            score_text.setText(""+intent.getIntExtra("score",0));
            score_text.setTextColor(Color.rgb(0,255,255));
        }else{
            score_text.setText(""+intent.getIntExtra("score",0));
        }

        sec = (int) (time_past / 1000) % 60;
        min = (int) time_past / 1000 / 60;
        milsec = (int) time_past / 10 % 100;
        time_text.setText(min + ":" + sec + "." + milsec);

        //UDPClient udpClient = new UDPClient(intent.getIntExtra("min", 0) + "m " + intent.getIntExtra("sec",0) + "s " + intent.getIntExtra("milsec",0),""+intent.getIntExtra("score",0),""+intent.getIntExtra("level",0),"임시 유져 Joes");
        //udpClient.start();
        /*
        try {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddress = InetAddress.getByName("121.176.112.220");

            byte[] msg = new byte[100];

            DatagramPacket outPacket = new DatagramPacket(msg, 1, serverAddress, 7777);
            DatagramPacket inPacket = new DatagramPacket(msg, msg.length);

            datagramSocket.send(outPacket);     // 전송
            datagramSocket.receive(inPacket);   // 수신

            serverdata.setText("current server time : " + new String(inPacket.getData()));

            datagramSocket.close();

        }catch(Exception e){
            throw new RuntimeException(e);
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gme_over, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            stopService(new Intent(this, BgmService.class));
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onStartClick(View v){
        startActivity(new Intent(this,inGame.class));
        finish();
    }

    public void onSettingClick(View v){
        setting_page = (LinearLayout)mInflater.inflate(R.layout.setting_layout,null);
        setting_page.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ((ViewManager)v.getParent()).removeView(v);
            }
        });
        ToggleButton music_toggle = (ToggleButton)setting_page.findViewById(R.id.music_toggle);
        music_toggle.setChecked(pref.getBoolean("mode_mute",false));
        music_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                editor.putBoolean("mode_mute", isChecked);
                editor.commit();
                if (isChecked) {
                    stopService(new Intent(getApplicationContext(), BgmService.class));
                } else {
                    startService(new Intent(getApplicationContext(), BgmService.class));
                }
            }
        });
        bg_layout.addView(setting_page, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        if(hasFocus&&!rotating){

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(bg_layout.getWidth(), bg_layout.getWidth());
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            background.setLayoutParams(params);
            background.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.background_rotation));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        startService(new Intent(this,BgmService.class));
    }
}
