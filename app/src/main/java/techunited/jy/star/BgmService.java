package techunited.jy.star;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.view.KeyEvent;

public class BgmService extends Service {
    public MediaPlayer mp;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        mp = MediaPlayer.create(this, R.raw.bgm);
        mp.setLooping(true); // 반복 재생 설정 (true와 false로 조정 가능)
        mp.start(); //음악 재생

    }

    public void onDestroy() {
        super.onDestroy();
        mp.stop(); //음악 정지
    }


}