package techunited.jy.star;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class inGame extends AppCompatActivity {

    int count = 0, starspawnX, starspawnY;


    final int time_before_start = 3000, speed = 600;
    final int NORMAL_STAR = 0, COUNT_STAR = 1, TIMER_STAR = 2, DISTURBANCE_STAR = 3, BOMB_STAR = 4,MINUS_STAR=5;
    int starKind[] = {1,1,2,2,2,3,3,4,4,4,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6};
    int lvSpeed[]={25,50,50,75,100,125,125,150,150,175,200,200,225,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600};

    int maxStar = 10;
    int life = 3;

    int h_score;

    LayoutInflater mInflater;

    int level, score;
    long starttime, time_past, time;

    Random rand;
    boolean run = false, game_started = false, end_game = false, temp = false, disturbing = false, stop_while_disturbing = false, new_h_score = false;

    RelativeLayout layout, pause_layout, bg_layout;
    LinearLayout dis_layout;

    TextView score_text, count_view, level_view, life_view;
    ImageView countView, resumeView, background,life_image;
    ProgressBar time_left;

    Animation speedup, dis_anim;

    TimerTask runnable;

    Object star_temp;
    int star_var;

    Runnable starmaker = new Runnable() {
        @Override
        public void run() {
            while (!end_game) {

                try {
                    Thread.sleep(speed - lvSpeed[level-1]);
                    if (temp) {
                        star_var = randomStar();
                        if(star_var==DISTURBANCE_STAR||star_var==BOMB_STAR||star_var==MINUS_STAR){
                            star_var = randomStar();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                switch (star_var) {
                                    case NORMAL_STAR:
                                        ((normalStar) star_temp).makeStar();
                                        break;
                                    case COUNT_STAR:
                                        ((countStar) star_temp).makeStar();
                                        break;
                                    case TIMER_STAR:
                                        ((timerStar) star_temp).makeStar();
                                        break;
                                    case DISTURBANCE_STAR:
                                        ((disturbanceStar) star_temp).makeStar();
                                        break;
                                    case BOMB_STAR:
                                        ((bombStar)star_temp).makeStar();
                                        break;
                                    case MINUS_STAR:
                                        ((minusStar)star_temp).makeStar();
                                        break;
                                }
                            }
                        });
                    }

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    };

    private int randomStar() {
        int temp = rand.nextInt(starKind[level-1]);
        switch (temp) {
            case NORMAL_STAR:
                star_temp = new normalStar(mInflater);
                return NORMAL_STAR;
            case COUNT_STAR:
                star_temp = new countStar(mInflater, 2);
                return COUNT_STAR;
            case TIMER_STAR:
                star_temp = new timerStar(mInflater, 3);
                return TIMER_STAR;
            case DISTURBANCE_STAR:
                star_temp = new disturbanceStar(mInflater, 2);
                return DISTURBANCE_STAR;
            case BOMB_STAR:
                star_temp = new bombStar(mInflater,2);
                return BOMB_STAR;
            case MINUS_STAR:
                star_temp = new minusStar(mInflater,3);
                return MINUS_STAR;
        }
        return -1;
    }

    Timer levelTimer;
    Thread starTimer;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    ColorStateList oldColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_game);


        speedup = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.speedup_anim);
        dis_anim = AnimationUtils.loadAnimation(this, R.anim.disturbance_anim);

        rand = new Random();
        starTimer = new Thread(starmaker);
        setRunnable();

        pref = getSharedPreferences("pref", MODE_PRIVATE);
        editor = pref.edit();
        h_score = pref.getInt("h_score", 0);

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);


        count_view = (TextView) findViewById(R.id.items);
        level_view = (TextView) findViewById(R.id.level);
        score_text = (TextView) findViewById(R.id.score);
        background = (ImageView) findViewById(R.id.background);
        life_view = (TextView) findViewById(R.id.life);
        life_image = (ImageView)findViewById(R.id.life_red);
        countView = (ImageView) findViewById(R.id.countview);
        layout = (RelativeLayout) findViewById(R.id.starspawn);
        time_left=(ProgressBar)findViewById(R.id.time_left_progress);
        bg_layout = (RelativeLayout) findViewById(R.id.game_layout);
        dis_layout = (LinearLayout) findViewById(R.id.dis_layout);


        dis_layout.bringToFront();


        level_view.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/mechanik_regular.ttf"));
        score_text.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/mechanik_regular.ttf"));

        mInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);


        pause_layout = (RelativeLayout) mInflater.inflate(R.layout.game_pause, null);
        resumeView = (ImageView) pause_layout.findViewById(R.id.resume);
        resumeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resumeGame();
            }
        });

        dis_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                disturbing = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                disturbing = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        oldColor = score_text.getTextColors();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_in_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void gameOver() {
        temp = false;
        game_started = false;
        end_game = true;
        levelTimer.cancel();
        layout.removeAllViews();
        count = 0;
        Intent intent = new Intent(this, gameOver.class);
        intent.putExtra("time_past", time_past);
        intent.putExtra("score", score);
        intent.putExtra("level", level);
        intent.putExtra("new_h_score", new_h_score);
        startActivity(intent);
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !run) {

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(bg_layout.getWidth(), bg_layout.getWidth());
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            background.setLayoutParams(params);
            background.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.background_rotation));

            starspawnX = layout.getWidth() - BitmapFactory.decodeResource(this.getResources(), R.drawable.star).getWidth();
            starspawnY = layout.getHeight() - BitmapFactory.decodeResource(this.getResources(), R.drawable.star).getHeight();


            starttime = System.currentTimeMillis() + time_before_start;
            levelTimer = new Timer();
            levelTimer.schedule(runnable, 0, 50);
        } else if (!hasFocus&&game_started) {
            pauseGame();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        temp = false;
        game_started = false;
        end_game = true;
        levelTimer.cancel();
        count = 0;
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseGame();
    }

    public void pauseGame() {
        if (temp) {
            game_started = false;
            temp = false;
            levelTimer.cancel();
            time = time_past;
            addContentView(pause_layout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            if (disturbing) {
                stop_while_disturbing = true;
                disturbing = false;
                dis_layout.clearAnimation();
                dis_layout.setAlpha(1.0f);
            }
        }
    }

    public void resumeGame() {
        ((ViewManager) pause_layout.getParent()).removeView(pause_layout);
        levelTimer = new Timer();
        setRunnable();
        starttime = System.currentTimeMillis();
        levelTimer.schedule(runnable, 0, 50);
        if (stop_while_disturbing) {
            dis_layout.setAlpha(1.0f);
            dis_layout.setVisibility(View.VISIBLE);
            dis_layout.startAnimation(dis_anim);
            stop_while_disturbing = false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (game_started && temp) {
                pauseGame();
            } else if (game_started) {
                resumeGame();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setRunnable() {
        runnable = new TimerTask() {
            @Override
            public void run() {

                try {
                    if (!run && !game_started) {
                        run = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                countView.setBackgroundResource(R.drawable.three);
                                countView.startAnimation(speedup);
                            }
                        });

                        Thread.sleep(1000);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                countView.setBackgroundResource(R.drawable.two);
                                countView.startAnimation(speedup);
                            }
                        });
                        Thread.sleep(1000);


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                countView.setBackgroundResource(R.drawable.one);
                                countView.startAnimation(speedup);
                            }
                        });
                        Thread.sleep(1000);
                        starTimer.start();

                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                temp = true;
                game_started = true;


                int temp_level = level;
                long curtime = System.currentTimeMillis();
                time_past = curtime - starttime + time;

                level = (int) time_past / 1000 / 10 + 1;
                time_left.setProgress((int)(time_past%10000)/100);
                if (temp_level != level && level != 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            countView.setBackgroundResource(R.drawable.speedup);
                            countView.setVisibility(View.VISIBLE);
                            countView.startAnimation(speedup);
                        }
                    });
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        level_view.setText("Lv : " + level);
                    }
                });
            }
        };
    }


    public class countStar {
        private ImageView mView;
        private TextView mCountView;
        private RelativeLayout count_star;
        private int counter;
        private Animation popin, popout;
        private Context context;

        public countStar(LayoutInflater mInflater, int count) {
            context = getApplicationContext();
            count_star = (RelativeLayout) mInflater.inflate(R.layout.count_star, null);
            mCountView = (TextView) count_star.findViewById(R.id.star_counter);
            mCountView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/mechanik_regular.ttf"));
            mView = (ImageView) count_star.findViewById(R.id.count_star);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
            this.counter = count;
        }

        public void makeStar() {
            if (count >= maxStar) {
                gameOver();
            } else {
                mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        counter -= 1;
                        score += time_past / 100;
                        updateScore();
                        if (counter == 0) {
                            count -= 1;
                            ((RelativeLayout) v.getParent()).startAnimation(popout);
                            ((ViewGroup) v.getParent()).removeView(v);
                            ((ViewGroup) v.getParent().getParent()).removeView((RelativeLayout) v.getParent());
                        } else if (counter == 1) {
                            ((ViewGroup) v.getParent()).removeView(mCountView);
                        } else {
                            mCountView.setText("" + counter);
                        }
                    }
                });

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
                layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
                Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);

                mCountView.setText("" + counter);
                layout.addView(count_star, layoutParams);
                count_star.startAnimation(popin);
                count += 1;
                count_view.setText("" + count);
            }//else
        }
    }

    public class normalStar {
        private ImageView mView;
        private Animation popin, popout;
        private Context context;

        public normalStar(LayoutInflater mInflater) {
            context = getApplicationContext();
            mView = (ImageView) mInflater.inflate(R.layout.normal_star, null);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
        }

        public void makeStar() {
            if (count >= maxStar) {
                gameOver();
            } else {
                mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        count -= 1;
                        score += time_past / 100;
                        updateScore();
                        count_view.setText("" + count);

                        v.startAnimation(popout);
                        ((ViewGroup) v.getParent()).removeView(v);
                    }
                });

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
                layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
                Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);
                //addContentView(mView, layoutParams);

                layout.addView(mView, layoutParams);
                mView.startAnimation(popin);
                count += 1;
                count_view.setText("" + count);
            }//else
        }
    }

    public class timerStar {
        private ImageView mView;
        private TextView mTimerView;
        private RelativeLayout timer_star;
        private int time, time_temp = 0;
        private Animation anim, popin, popout;
        private Timer timer;
        private Context context;

        public timerStar(LayoutInflater mInflater, int time) {
            context = getApplicationContext();
            anim = AnimationUtils.loadAnimation(context, R.anim.explode_anim);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
            this.time = time;
            timer_star = (RelativeLayout) mInflater.inflate(R.layout.timer_star, null);
            mView = (ImageView) timer_star.findViewById(R.id.timer_star);
            mTimerView = (TextView) timer_star.findViewById(R.id.star_timer);
            mTimerView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/mechanik_regular.ttf"));
        }

        public void makeStar() {

            timer = new Timer();
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timer.cancel();
                    score += time_past / 100;
                    updateScore();
                    try {
                        ((RelativeLayout) v.getParent()).startAnimation(popout);
                        ((ViewGroup) v.getParent()).removeView(v);
                        ((ViewGroup) v.getParent().getParent()).removeView((RelativeLayout) v.getParent());
                    }catch(Exception e){
                        Log.d("timer_makeStar","Crash!");
                    }
                }
            });

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
            layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
            Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);


            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (temp) {
                        time_temp += 1;
                        if (time_temp >= 10) {
                            time -= 1;
                            time_temp = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (time <= 0) {
                                            timer_star.removeView(mTimerView);
                                            timer.cancel();
                                            explode();
                                        } else {
                                            mTimerView.setText("" + time);
                                        }
                                    }catch(Exception e){
                                        Log.d("timer_timer","Crash");
                                    }
                                }
                            });
                        }
                    }
                }
            }, 100, 100);
            mTimerView.setText("" + time);
            layout.addView(timer_star, layoutParams);
            timer_star.startAnimation(popin);
            count_view.setText("" + count);

        }

        public void explode() {
            updateLife(true);
            try {
                timer_star.startAnimation(anim);
                ((ViewGroup) timer_star.getParent()).removeView(timer_star);
            }catch(Exception e){
                Log.d("timer_explode","Crash!");
            }
        }
    }

    public class disturbanceStar {
        private ImageView mView;
        private int time, time_temp;
        private Timer timer;
        private Animation popin, popout;
        private Context context;

        public disturbanceStar(LayoutInflater mInflater, int time) {
            this.time = time;
            context = getApplicationContext();
            mView = (ImageView) mInflater.inflate(R.layout.disturbance_star, null);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
        }

        public void makeStar() {

            timer = new Timer();
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timer.cancel();
                    try {
                        v.startAnimation(popout);
                        ((ViewGroup) v.getParent()).removeView(v);
                    }catch(Exception e){
                        Log.d("dis_makeStar","Crash!");
                    }
                    disturb();
                }
            });

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
            layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
            Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);


            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (temp) {
                        time_temp += 1;
                        if (time_temp >= 10) {
                            time -= 1;
                            time_temp = 0;
                            if (time <= 0) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            mView.startAnimation(popout);
                                            layout.removeView(mView);
                                        }catch(Exception e){
                                            Log.d("dis_timer","Crash!");
                                        }
                                        timer.cancel();
                                    }
                                });
                            }
                        }
                    }
                }
            }, 100, 100);

            layout.addView(mView, layoutParams);
            mView.startAnimation(popin);

        }

        public void disturb() {
            dis_layout.setAlpha(1.0f);
            dis_layout.setVisibility(View.VISIBLE);
            dis_layout.startAnimation(dis_anim);
        }
    }

    public class bombStar {
        private ImageView mView;
        private Animation anim, popin, popout;
        private Timer timer;
        private int time, time_temp;
        private Context context;

        public bombStar(LayoutInflater mInflater, int time) {
            context = getApplicationContext();
            this.time = time;
            timer = new Timer();
            mView = (ImageView) mInflater.inflate(R.layout.bomb_star, null);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
            anim = AnimationUtils.loadAnimation(context, R.anim.explode_anim);
        }

        public void makeStar() {

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timer.cancel();
                    updateLife(true);
                    try {
                        v.startAnimation(anim);
                        ((ViewGroup) v.getParent()).removeView(v);
                    }catch(Exception e){
                        Log.d("bomb_makeStar","Crash");
                    }
                }
            });

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
            layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
            Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);
            //addContentView(mView, layoutParams);


            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (temp) {
                        time_temp += 1;
                        if (time_temp >= 10) {
                            time -= 1;
                            time_temp = 0;
                            if (time <= 0) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            mView.startAnimation(popout);
                                            layout.removeView(mView);
                                        }catch(Exception e){
                                            Log.d("bomb_timer","Crash");
                                        }
                                        timer.cancel();
                                    }
                                });
                            }
                        }
                    }
                }
            }, 100, 100);

            layout.addView(mView, layoutParams);
            mView.startAnimation(popin);
        }
    }

    public class minusStar{
        private ImageView mView;
        private Animation anim, popin, popout;
        private Timer timer;
        private int time, time_temp;
        private Context context;

        public minusStar(LayoutInflater mInflater, int time) {
            context = getApplicationContext();
            this.time = time;
            timer = new Timer();
            mView = (ImageView) mInflater.inflate(R.layout.minus_star, null);
            popin = AnimationUtils.loadAnimation(context, R.anim.popin);
            popout = AnimationUtils.loadAnimation(context, R.anim.popout);
            anim = AnimationUtils.loadAnimation(context, R.anim.explode_anim);
        }

        public void makeStar() {

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    timer.cancel();
                    score-=5000;
                    updateScore();
                    try {
                        v.startAnimation(anim);
                        ((ViewGroup) v.getParent()).removeView(v);
                    }catch(Exception e){
                        Log.d("minus_makeStar","Crash");
                    }
                }
            });

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = (int) (rand.nextFloat() * starspawnX);
            layoutParams.topMargin = (int) (rand.nextFloat() * starspawnY);
            Log.d("makeStar", "" + layoutParams.leftMargin + " " + layoutParams.topMargin);
            //addContentView(mView, layoutParams);


            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (temp) {
                        time_temp += 1;
                        if (time_temp >= 10) {
                            time -= 1;
                            time_temp = 0;
                            if (time <= 0) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            mView.startAnimation(popout);
                                            layout.removeView(mView);
                                        }catch(Exception e){
                                            Log.d("minus_timer","Crash");
                                        }
                                        timer.cancel();
                                    }
                                });
                            }
                        }
                    }
                }
            }, 100, 100);

            layout.addView(mView, layoutParams);
            mView.startAnimation(popin);
        }
    }

    public void onPauseClick(View v) {
        if (game_started)
            pauseGame();
    }

    public void updateScore() {
        if (score >= h_score) {
            new_h_score = true;
            score_text.setText("" + score + "  HIGH");
            score_text.setTextColor(Color.rgb(0, 0, 255));
            editor.putInt("h_score", score);
            editor.putInt("lv", level);
            editor.commit();
        } else {
            score_text.setText("" + score);
            score_text.setTextColor(oldColor);
        }
    }

    public void updateLife(boolean lifeDown){
        if(lifeDown){
            life-=1;
        }
        if(life<=1) {
            life_view.setText("X " + life);
            life_view.setTextColor(Color.rgb(255,0,0));
        }else{
            life_view.setText("X "+life);
        }
        if(life<=0){
            gameOver();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        startService(new Intent(this,BgmService.class));
    }
}
