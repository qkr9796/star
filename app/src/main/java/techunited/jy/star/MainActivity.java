package techunited.jy.star;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewManager;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context,Intent intent){
            stopService(new Intent(getApplicationContext(),BgmService.class));
        }
    };

    ImageView background;
    RelativeLayout bg_layout;
    boolean rotating=false;
    LayoutInflater mInflater;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int h_score,level;
    boolean backConfirm = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, filter);

        mInflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
        pref = getSharedPreferences("pref",MODE_PRIVATE);
        editor = pref.edit();
        h_score=pref.getInt("h_score", 0);
        level = pref.getInt("lv",0);

        bg_layout=(RelativeLayout)findViewById(R.id.bg_layout);
        background=(ImageView)findViewById(R.id.background);

    }

    public void onResume(){
        super.onResume();
        if(!pref.getBoolean("mode_mute",false)) {
            Log.d("mainAcitivity","mute_mode"+pref.getBoolean("mute_mode",false));
            startService(new Intent(this, BgmService.class));
        }
        backConfirm = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onPlayClick(View v){
        Intent intent = new Intent(this,inGame.class);
        startActivity(intent);
    }

    public void onSettingClick(View v){
        LinearLayout setting_page = (LinearLayout)mInflater.inflate(R.layout.setting_layout,null);
        setting_page.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ((ViewManager)v.getParent()).removeView(v);
            }
        });
        ToggleButton music_toggle = (ToggleButton)setting_page.findViewById(R.id.music_toggle);
        music_toggle.setChecked(pref.getBoolean("mode_mute",false));
        music_toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                editor.putBoolean("mode_mute", isChecked);
                editor.commit();
                if (isChecked) {
                    stopService(new Intent(getApplicationContext(), BgmService.class));
                } else {
                    startService(new Intent(getApplicationContext(), BgmService.class));
                }
            }
        });
        bg_layout.addView(setting_page,new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
    }

    public void onScoreClick(View v){
        LinearLayout high_score_page = (LinearLayout)mInflater.inflate(R.layout.score_layout,null);
        high_score_page.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ((ViewManager)v.getParent()).removeView(v);
            }
        });
        h_score = pref.getInt("h_score",0);
        level = pref.getInt("lv",0);
        TextView h_score_view = (TextView)high_score_page.findViewById(R.id.h_score_show);
        TextView lv = (TextView)high_score_page.findViewById(R.id.h_level_show);
        TextView tv1 = (TextView)high_score_page.findViewById(R.id.tv1);
        TextView tv2 = (TextView)high_score_page.findViewById(R.id.tv2);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/mechanik_regular.ttf");
        h_score_view.setTypeface(font);
        lv.setTypeface(font);
        tv1.setTypeface(font);
        tv2.setTypeface(font);
        h_score_view.setText("" + h_score);
        lv.setText(""+level);
        bg_layout.addView(high_score_page, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
    }

    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(receiver);
        stopService(new Intent(this, BgmService.class));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_HOME) {
            Toast.makeText(this,"Home",Toast.LENGTH_SHORT).show();
            stopService(new Intent(this, BgmService.class));
        }else if(keyCode==KeyEvent.KEYCODE_BACK){
            if(backConfirm){
            }else{
                backConfirm = true;
                Toast.makeText(this,"Press 'Back' Again to Leave",Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        if(hasFocus&&!rotating){

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(bg_layout.getWidth(), bg_layout.getWidth());
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            background.setLayoutParams(params);
            background.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.background_rotation));
        }
    }

}
