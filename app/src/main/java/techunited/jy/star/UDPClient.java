package techunited.jy.star;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by Administrator on 2015-11-02.
 */
public class UDPClient extends Thread {

    String time,score,level,name;

    public UDPClient(String time, String score, String level, String name) {
        super();
        this.time = time;
        this.score = score;
        this.level = level;
        this.name=name;
    }

    public void run() {
        try
        {
                // 전송할 데이터 : 현재 시간
                String data = String.valueOf(System.currentTimeMillis())+"\n"+time+"\n"+score+"\n"+level+"\n"+name;

                // 전송할  DatagramPacket 생성
                DatagramPacket packet = new DatagramPacket(data.getBytes(), data.getBytes().length, InetAddress
                        .getByName("121.176.112.220"), 7777);

                // DatagramSocket 생성
                DatagramSocket socket = new DatagramSocket();

                // DatagramPacket 전송
                socket.send(packet);

        } catch (UnknownHostException e)
        {
            e.printStackTrace();
        } catch (SocketException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
